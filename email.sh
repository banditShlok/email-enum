#!/bin/bash

source ./keys

if [ -z $1 ]
then
	echo "[-] Give the organization name"
	echo "./email.sh [org]"
	echo "Example - ./email.sh example.com"
	exit 1
fi

intelx_id=$(curl -sSL -X POST "https://2.intelx.io/phonebook/search?k=$PHONEBOOK_KEY" -d '{"term":"'$1'","maxresults":1000000,"media":0,"target":2,"terminate":[null],"timeout":20}'|jq .id|cut -d "\"" -f2)
curl -sSL "https://2.intelx.io/phonebook/search/result?k=$PHONEBOOK_KEY&id=$intelx_id&limit=1000000" | jq . | grep selectorvalue | cut -d "\"" -f 4 | sort -u > emails-$1.txt

./teams-enum userenum -f emails-$1.txt -t "$TEAMS_AUTH_TOKEN" | grep '\[+\]' | cut -d ' ' -f 2 > verified_email-$1.txt
