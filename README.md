# email-enum

Enumerating emails with verification

## Getting started

### Installation

```
git clone https://gitlab.com/banditShlok/email-enum.git
cd email-enum
```

### Configuration

Configure your IntelX Phonebook API Key and Teams User Authentication token in the `keys` file

```
export PHONEBOOK_KEY=""
export TEAMS_AUTH_TOKEN=""
```

### Usage

```
./email.sh [org]
```

**Example** - 

```
./email.sh example.com
```

**Note** : All the emails are saved in the file `emails-[org].txt` and the verified emails are saved into `verified-emails-[org].txt` 
